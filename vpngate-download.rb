# Downloads list of VPN's from vpngate and outputs in json
require 'csv'
require 'json'

FILE = "vpngate-server-list.csv"

%x(curl -o #{FILE} http://www.vpngate.net/api/iphone/)

output = []

# Open CSV File
i = 0
CSV.foreach(FILE) do |row|
  if(i > 1 && row[0]!="*")
    output << {
      name: row[0],
      ip: row[1],
      country: row[6],
      ping: row[3],
      speed: row[4],
      config: row[14]
    }
  end
  i += 1
end

puts output.to_json
