FROM debian

RUN apt-get update \
 && apt-get install -y curl vim traceroute telnet openvpn \
 && rm -rf /var/lib/apt/lists/*
 
COPY . /usr/src/app
WORKDIR /usr/src/app
 
CMD sh ./startup.sh
