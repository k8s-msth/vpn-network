# A wrapper script around the various scripts to create a VPNGate cpn client in the cluster.

SERVER_LIST = "vpngate-server-list.json"
COUNTRY = ARGV.shift

if !COUNTRY
  puts "Usage: deploy.rb <country-code>"
  exit 1
end

# Setup
configmap = COUNTRY.downcase
puts %x(mkdir -p deployments/#{COUNTRY})
puts %x(kubectl -n vpn delete cm #{configmap})
puts %x(kubectl -n vpn delete deploy #{configmap})

puts %x(cat #{SERVER_LIST} | jq -r '[.[] | select(.country | contains("#{COUNTRY}"))]' | jq -r '.[0].config' | base64 --decode > deployments/#{COUNTRY}/client.ovpn)

puts %x(kubectl -n vpn create cm #{configmap} --from-file=deployments/#{COUNTRY}/client.ovpn)

puts %x(ruby create_deployment.rb #{configmap} > deployments/#{COUNTRY}/deployment-#{configmap}.yaml)

puts %x(kubectl -n vpn create -f deployments/#{COUNTRY}/deployment-#{configmap}.yaml)

puts %x(kubectl -n vpn expose deploy #{COUNTRY})
