# VPN Network


## VPN Gate

ServerList:

`curl -o vpngate-servers http://www.vpngate.net/api/iphone/`


## Creating a vpn deployment in the cluster


Download the latest list from VPNGate

  `vpngate-download.rb > vpngate-server-list.json`


Filter out the first listed vpn in FR, decode it and write to client.ovpn:

  `cat vpngate-server-list.json | jq -r '[.[] | select(.country | contains("FR"))]' | jq -r '.[1].config' | base64 --decode > client.ovpn`


Create a configmap from the file client.ovpn

  `kubectl -n vpn create cm fr-1 --from-file=client.ovpn`


Create a deployment with a specified name

  `create_deployment.rb fr-1 > deployment-fr-1.yaml`


Create the deployment

  `kubectl -n vpn create -f deployment-fr-1.yaml`


Expose the deployment

  `kubectl -n vpn expose deploy fr-1`

