# Creates a deployment
require 'erb'
require 'ostruct'

DEPLOYMENT = "deployment.yaml.erb"
NAME = ARGV.shift

if !NAME
  puts "Usage: create_deployment.rb <name>"
  exit 1
end

class Deployment < OpenStruct
  def render
    ERB.new(File.read(DEPLOYMENT)).result(binding)
  end
end

puts Deployment.new(:name =>  NAME).render
