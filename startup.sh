#!/bin/sh
 
mkdir -p /dev/net
mknod /dev/net/tun c 10 200
chmod 600 /dev/net/tun
cd /etc/openvpn/
sleep 10

# After connecting to VPN we cannot connect to the k8s NS anymore
# Adding route to mitigate this issue
export NS=`cat /etc/resolv.conf | grep nameserver | awk '{print $2}'`
export ETH0=`ip route list | grep default | awk '{ print $3 }'`
# Add route to DNS
ip route add $NS via $ETH0 dev eth0
# Add route to kubernetes services IP range
ip route add 10.96.0.0/12 via $ETH0 dev eth0
openvpn --config /usr/src/app/client.ovpn
